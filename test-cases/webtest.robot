*** Settings ***
Library  SeleniumLibrary
Library  Process

Suite Setup  Test suite setup
Suite Teardown  Test suite teardown

*** Variables ***
${server_py}            ${EXECDIR}/demoapp/server.py
${login_url}            http://127.0.0.1:9898/
${welcome_url}          ${login_url}welcome.html
${error_url}            ${login_url}error.html
${username}             admin
${password}             changeme
${default_browser}      headlessfirefox

*** Keywords ***
Setup Browser
    [Arguments]
    ...  ${browser}=${default_browser}
    log  ${browser}

    Run Keyword And Ignore Error  close all browsers
    Open Browser   browser=${browser}
    Set Window Position  ${0}  ${0}
    Set Window Size  ${1280}  ${800}

Test suite setup
    ${server_handle}=  Process.Start Process  python  ${server_py}
    Set Suite Variable  ${server_handle}
    setup browser

Test suite teardown
    Process.Terminate Process  ${server_handle}
    Process.Terminate All Processes
    Close All Browsers  

Login server
    [Arguments]
    ...  ${userid}=${username}
    ...  ${passwd}=${password}
    Go To  url=${login_url}
    Location Should Be  url=${login_url}
    Input Text  id:username_field  ${userid}
    Input Text  id:password_field  ${passwd}
    Click Button  id:login_button
    Sleep  1s  reason=Wait for page refresh
    wait until Page Does Not Contain Element  id:login_button  timeout=10s

Login server with invalid account
    [Arguments]
    ...  ${username}=${EMPTY}
    ...  ${password}=${EMPTY}
    log  ${username} ${password}
    Login server  userid=${username}  passwd=${password}
    Location Should Be  url=${error_url}
    Capture Page Screenshot

*** Test Cases ***
Login Server with valid account
    Login server 
    Location Should Be  url=${welcome_url}
    Capture Page Screenshot

Logout
    Login server
    Click Link  link:logout
    wait Until Page Does Not Contain Element  link:logout
    Page Should Contain  Login Page
    Capture Page Screenshot

Login Server with invalid accounts
    [Template]  Login server with invalid account

    #--username--#--password--#
    1               2
    xyz             abc
    ${EMPTY}        ${EMPTY}








